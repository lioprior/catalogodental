// Hacer un require del módulo express, nuestro web framework por excelencia
var express = require('express');
// Construimos el objeto express
var app = new express();

var compression		= require('compression');
var bodyParser 		= require('body-parser');
var morgan 			= require('morgan');
var cookieParser 	= require('cookie-parser');

// Ocupamos el módulo express-session para gestionar sesiones
var session			= require('express-session');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(morgan('dev')); 		
app.use(compression());


// Configuramos express para que agregue un middleware de sessión
app.use(session({

	secret: "s14f8sd!",			// Generar encriptación
	saveUninitialized: 	false,	// No genera sesiones automáticamente
	resave: 			false 	 
}));

app.use(express.static(__dirname + '/public'));


require('./routes/api')(app);
require('./routes/www')(app);
require('./routes/auth')(app);



// Ponemos a escuchar el servidor
app.listen(8080, function () {

	console.log('Escuchando');
});